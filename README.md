> Media team's fork, fixing own mmcore.request() implementation [Request](https://bitbucket.org/gtsmediateam/essentials-request).
> **Note**: master (jQuery) branch is no more supported.

Maxymiser Core - Essentails
===========================

A bundle of the essential mmcore extensions:

- [Try Catch](https://bitbucket.org/gamingteam/essentials-trycatch)
- [Campaign](https://bitbucket.org/gamingteam/essentials-campaign)
- [Mediator](https://bitbucket.org/gamingteam/essentials-mediator)
- [Wait for](https://bitbucket.org/gamingteam/essentials-waitfor)
- [Deferred](https://bitbucket.org/gamingteam/essentials-deferred)

Installation
------------

1. Create a site script, and put the contents of [essentials.min.js](http://gitlab.maxymiser.net/gts/mmcore-essentials/raw/master/out/essentials.min.js) there.
2. Create an overlay page that covers all the site pages. Most likely it has already been created.
3. Map the script with the lowest order (it must be executed before other scripts, so they can immediately use it) to the page.

Dependencies
------------

No dependencies ([jQuery Arrival](http://gitlab.maxymiser.net/gts/mmcore-jquery-arrival) loads jQuery if it is not loaded).
