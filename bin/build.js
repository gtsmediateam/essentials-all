/**
 * @fileoverview Writes minimized versions of the essential mmcore extentions
 * (except Attach Style that is plugged in a different way) into a single file
 * that is mapped to all pages on the site where the extensions are required.
 *
 * @author evgeniy@pavlyuk.me (Evgeniy Pavlyuk)
 */

'use strict';

var path = require('path');
var fs = require('fs');

var REPOSITORY_DIRNAME = path.normalize(path.join(__dirname, '..'));

var outputStream = new fs.WriteStream('essentials.min.js');

// First, include the try-catch extension.
writeSubmodulesIntoOutputStream(['try-catch'], function() {
  // Include other extensions in a try-catch wrap.
  outputStream.write('mmcore.tryCatch(function(){\'use strict\';\n\n');
  writeSubmodulesIntoOutputStream(['jquery-arrival'], function() {
    outputStream.write('mmcore.addJQueryArrivalCallback(function() {');
    writeSubmodulesIntoOutputStream([
      // Listed in the order required to resolve dependencies.
      'request',
      'mediator',
      'wait-for',
      'wait-for-element-arrival',
      'campaign',
      'set-action-criterion'
    ], function() {
      outputStream.end('});\n\n})();');
    });
  });
});

function writeSubmodulesIntoOutputStream(submoduleNames, callback) {
  if (!submoduleNames.length) {
    if (typeof callback === 'function') {
      callback();
    }
    return;
  }

  var name = submoduleNames.shift();

  // All the extensions are located in the submodules directory, and has the
  // same file name and structure format. 
  var filename = path.join(REPOSITORY_DIRNAME, 'submodules', name,
                           'src', name + '.min.js');
  var readStream = new fs.ReadStream(filename);

  // Write the minimized source code into the output file, but do not close
  // the output stream at the end.
  readStream.pipe(outputStream, {end: false});

  // When the source code is written, write the next one.
  readStream.on('end', function() {
    outputStream.write('\n');
    writeSubmodulesIntoOutputStream(submoduleNames, callback);
  });
}
